<?php

/***
 * Template Name: Privacy Policy  Page Template
 */
get_header();
?>

<section class="p-rel privacy-policy-p">
    <div class="container join">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <h2 class="heading"> Privacy Policy | Razorpay Payments </h2>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-md-9">
                <?php echo get_the_content(); ?>
            </div>
        </div>
    </div>
    <div class="circ"></div>

</section>

<?php
get_footer();
?>