<?php

/***
 * Template Name: Home Page Template
 */
get_header();
?>

<?php if (have_rows('banner_section')) : ?>
    <header class="pb-0">
        <?php while (have_rows('banner_section')) : the_row(); ?>
            <div class="container bannerText">
                <div class="row justify-content-center z-in2 p-rel">
                    <div class="col-lg-8">
                        <h2><?php echo get_sub_field('heading'); ?></h2>
                        <h1 style="font-size: 24px;color: #fff;"><?php echo get_sub_field('sub_heading'); ?></h1>
                        <p><?php echo get_sub_field('text'); ?></p>
                        <a class="book-btn"><?php echo get_sub_field('button'); ?></a>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <img loading="lazy" src="<?php echo get_template_directory_uri(); ?>/assets/img/halfCircle.png" class="half1 img-fluid" alt="half Circle" title="Performance Marketing" />
                    </div>
                </div>
            </div>
            <img loading="lazy" src="<?php echo get_sub_field('image'); ?>" class="coins" alt="Coins" title="Coins" />
            <p class="header-bgText">NEON</p>
        <?php endwhile; ?>
    </header>
<?php endif; ?>

<?php if (have_rows('strategy_section')) : ?>
    <?php while (have_rows('strategy_section')) : the_row(); ?>
        <section class="p-rel">
            <div class="container join">
                <div class="row justify-content-center">
                    <div class="col-md-7">
                        <p class="subheading"><?php echo get_sub_field('sub_heading'); ?></p>
                        <h2 class="heading">
                            <?php echo get_sub_field('heading'); ?>
                        </h2>
                    </div>
                </div>
                <div class="row justify-content-center">
                    <div class="col-md-9">
                        <p>
                            <?php echo get_sub_field('content'); ?>
                        </p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 text-center">
                        <a class="book-btn"><?php echo get_sub_field('button'); ?></a>
                    </div>
                </div>
            </div>
            <div class="circ"></div>
        </section>
    <?php endwhile; ?>
<?php endif; ?>




<!-- About Section -->

<?php if (have_rows('about_section')) : ?>
    <?php while (have_rows('about_section')) : the_row(); ?>
        <section class="bg1 p-rel z-in2">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <img loading="lazy" src="<?php echo get_template_directory_uri(); ?>/assets/img/halfCircle.png" class="half2 img-fluid" alt="Half Circle" title="Half Circle" />
                    </div>
                </div>
            </div>
            <div class="container abt-width">
                <div class="row">
                    <div class="col-lg-4 p-rel">
                        <p class="bg-N">N</p>
                        <p class="subheading"><?php echo get_sub_field('sub_heading'); ?></p>
                        <h2 class="heading"><?php echo get_sub_field('heading'); ?></h2>
                    </div>
                    <!-- ABout Carousel Here -->
                    <div class="col-lg-8 z-in2">
                        <div class="about-carousel">
                            <?php if (have_rows('about_slider')) : ?>
                                <?php while (have_rows('about_slider')) : the_row(); ?>
                                    <div class="slidess">
                                        <div class="carousel-indi">
                                            <div class="dot-border">
                                                <div class="circular-dot"></div>
                                            </div>
                                            <div class="hr-line"></div>
                                        </div>
                                        <p class="carousel-num"><?php echo get_sub_field('number'); ?></p>
                                        <h5 class="carousel-heading"><?php echo get_sub_field('heading'); ?></h5>
                                        <p class="carousel-content">
                                            <?php echo get_sub_field('content'); ?>
                                        </p>
                                    </div>
                                <?php endwhile; ?>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="circle2">
                <p class="bg-N2">N</p>
                <img loading="lazy" src="<?php echo get_sub_field('image'); ?>" class="magnet" alt="Performance Marketing In India" title="Performance Marketing Agency In India" />
            </div>
        </section>
    <?php endwhile; ?>
<?php endif; ?>




<!-- Our Clients Section -->

<?php if (have_rows('clients_section')) : ?>
    <?php while (have_rows('clients_section')) : the_row(); ?>
        <section class="our-clients">
            <div class="container">
                <div class="row">
                    <div class="col-12 p-rel">
                        <p class="bg-N3">N</p>
                        <p class="subheading"></p>
                        <h2 class="heading"><?php echo get_sub_field('heading'); ?></h2>
                    </div>
                </div>

                <?php $rows = get_sub_field('clients'); ?>
                <?php if ($rows) : ?>
                    <?php $count = count($rows);
                    // echo $count;
                    $n = round($count / 2); ?>
                    <div class="client-icons d-none d-md-block">
                        <?php $k = 1;
                        $j = 1;
                        $item = 0;
                        $lastJ = 1; ?>
                        <?php for ($i = 1; $item <= ($count - 1); $i++) : ?>
                            <div class="row justify-content-center">
                                <?php for ($k = 1; $k <= $j; $k++) : ?>
                                    <div class="col-md-2">
                                        <?php if ($k % 2 == 1) : ?>
                                            <img loading="lazy" src="<?php echo $rows[$item]['image']; ?>" class="bx-shdw" alt="tatamotors" title="tatamotors" />
                                            <?php $item++; ?>
                                        <?php endif; ?>
                                    </div>
                                <?php endfor; ?>
                                <?php //$lastJ = $j; 
                                ?>
                                <?php if ($j >= 1 && $j <= 5 && $lastJ <= $j) :
                                    $lastJ = $j;
                                    $j = $j + 2;
                                    if ($j > 5) :
                                        $j = $j - 4;
                                    endif;
                                elseif ($j >= 1 && $j <= 5 && $lastJ >= $j) :
                                    $lastJ = $j;
                                    $j = $j - 2;
                                    if ($lastJ == 1) :
                                        $j = 3;
                                    endif;
                                endif; ?>
                            </div>
                        <?php endfor; ?>
                    </div>
                <?php endif; ?>
                <!-- Our Clients mobile -->
                <?php if (have_rows('clients')) : ?>
                    <div class="client-icons d-block d-md-none">
                        <div class="row justify-content-center">
                            <?php while (have_rows('clients')) : the_row(); ?>
                                <div class="col-6">
                                    <img loading="lazy" src="<?php echo get_sub_field('image'); ?>" class="bx-shdw" alt="tatamotors" title="tatamotors" />
                                </div>
                            <?php endwhile; ?>
                        </div>
                    </div>
                <?php endif; ?>
            </div>
        </section>
    <?php endwhile; ?>
<?php endif; ?>

<!-- ANother About Section -->

<?php if (have_rows('who_we_are')) : ?>
    <?php while (have_rows('who_we_are')) : the_row(); ?>
        <section class="abt2-section">
            <div class="who-we-are-BG">
                <img loading="lazy" src="<?php echo get_sub_field('background_image'); ?>" class="img-fluid" alt="Performance Marketing Agency" title="Performance Marketing Agency in India" />
            </div>

            <div class="container" style="max-width: 1250px">
                <div class="row justify-content-between">
                    <div class="col-lg-3 orderr-2">
                        <div class="row">
                            <?php if (have_rows('point_one')) : ?>
                                <?php while (have_rows('point_one')) : the_row(); ?>
                                    <div class="col-12">
                                        <div class="top-icon-box">
                                            <img loading="lazy" src="<?php echo get_sub_field('image'); ?>" alt="Performance Marketing Agency" title="Performance Marketing Agency in India" />

                                            <h5><?php echo get_sub_field('heading'); ?></h5>

                                            <p>
                                                <?php echo get_sub_field('text'); ?>
                                            </p>
                                        </div>
                                    </div>
                                <?php endwhile; ?>
                            <?php endif; ?>

                            <?php if (have_rows('point_two')) : ?>
                                <?php while (have_rows('point_two')) : the_row(); ?>
                                    <div class="col-12">
                                        <div class="top-icon-box mt-50px">
                                            <img loading="lazy" src="<?php echo get_sub_field('image') ?>" alt="Performance Marketing Agency" title="Performance Marketing Agency in India" />

                                            <h5><?php echo get_sub_field('heading') ?></h5>

                                            <p>
                                                <?php echo get_sub_field('text') ?>
                                            </p>
                                        </div>
                                    </div>
                                <?php endwhile; ?>
                            <?php endif; ?>
                        </div>
                    </div>

                    <div class="col-lg-4 my-auto p-rel orderr-1">
                        <?php if (have_rows('point_three')) : ?>
                            <?php while (have_rows('point_three')) : the_row(); ?>
                                <div class="abt-neon2">
                                    <div class="aboutN">
                                        <img loading="lazy" src="<?php echo get_sub_field('image') ?>" alt="About NEON" title="About NEON" />
                                    </div>
                                    <p class="subheading"><?php echo get_sub_field('sub_heading') ?></p>
                                    <h2 class="heading"><?php echo get_sub_field('heading') ?></h2>
                                    <p class="abt-text">
                                        <?php echo get_sub_field('text') ?>
                                    </p>
                                    <a class="book-btn"><?php echo get_sub_field('button'); ?></a>
                                </div>
                            <?php endwhile; ?>
                        <?php endif; ?>
                    </div>

                    <div class="col-lg-3 orderr-3">
                        <div class="row">
                            <?php if (have_rows('point_four')) : ?>
                                <?php while (have_rows('point_four')) : the_row(); ?>
                                    <div class="col-12">
                                        <div class="top-icon-box mt-25px">
                                            <img loading="lazy" src="<?php echo get_sub_field('image') ?>" alt="We have a Soul" title="We have a Soul" />
                                            <h5><?php echo get_sub_field('heading') ?></h5>
                                            <p>
                                                <?php echo get_sub_field('text') ?>
                                            </p>
                                        </div>
                                    </div>
                                <?php endwhile; ?>
                            <?php endif; ?>

                            <?php if (have_rows('point_five')) : ?>
                                <?php while (have_rows('point_five')) : the_row(); ?>
                                    <div class="col-12">
                                        <div class="top-icon-box mt-50px">
                                            <img loading="lazy" src="<?php echo get_sub_field('image') ?>" alt="positive Approach" title="positive Approach" />
                                            <h5><?php echo get_sub_field('heading') ?></h5>
                                            <p>
                                                <?php echo get_sub_field('text') ?>
                                            </p>
                                        </div>
                                    </div>
                                <?php endwhile; ?>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    <?php endwhile; ?>
<?php endif; ?>

<!-- OUr Team Section -->

<?php if (have_rows('our_team')) : ?>
    <?php while (have_rows('our_team')) : the_row(); ?>
        <section class="our-team">
            <div class="circle3"></div>
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <p class="bg-N3">N</p>
                        <p class="subheading"><?php echo get_sub_field('sub_heading'); ?></p>
                        <h2 class="heading"><?php echo get_sub_field('heading'); ?></h2>
                    </div>
                </div>

                <div class="thisHide">
                    <div class="row justify-content-center">
                        <?php if (have_rows('team_section')) : ?>
                            <?php $i = 1; ?>
                            <?php while (have_rows('team_section')) : the_row(); ?>
                                <?php if ($i == 1) : ?><div class="col-md-3 col-9"></div><?php endif; ?>
                                <div class="col-md-3 col-9">
                                    <div class="top-img-box">
                                        <div style="position: relative">
                                            <div style="overflow: hidden">
                                                <img loading="lazy" src="<?php echo get_sub_field('image'); ?>" class="img-fluid" alt="Meher Patel" title="Meher Patel- Founder | Strategist" />
                                            </div>

                                            <?php
                                            $link = get_sub_field('linkedin_profile_link');
                                            if ($link) :
                                                $link_url = $link['url'];
                                                $link_title = $link['title'];
                                                $link_target = $link['target'] ? $link['target'] : '_self';
                                            ?>
                                                <a href=" <?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>">
                                                    <div class="linked"><i class="fab fa-linkedin-in"></i></div>
                                                </a>
                                            <?php endif; ?>

                                            <!-- <a href="https://www.linkedin.com/in/meherabad/">
                                                <div class="linked"><i class="fab fa-linkedin-in"></i></div>
                                            </a> -->
                                        </div>
                                        <h3><?php echo get_sub_field('name'); ?></h3>
                                        <p><?php echo get_sub_field('designation'); ?></p>
                                    </div>
                                </div>
                                <?php if ($i == 2) : ?><div class="col-md-3 col-9"></div><?php endif; ?>
                                <?php $i++; ?>
                            <?php endwhile; ?>
                        <?php endif; ?>
                    </div>

                    <div class="row">
                        <div class="col-12 text-center">
                            <button class="load-more" id="loadmore">Load More</button>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    <?php endwhile; ?>
<?php endif; ?>

<!-- WHat we do -->

<?php if (have_rows('services_section')) : ?>
    <?php while (have_rows('services_section')) : the_row(); ?>
        <section class="what-we-do">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-lg-6 text-center">
                        <div class="img-N">
                            <img loading="lazy" src="<?php echo get_sub_field('image'); ?>" alt="NEON Performance Marketing Services" title="NEON Performance Marketing Services" />
                        </div>
                        <p class="subheading"><?php echo get_sub_field('sub_heading'); ?></p>
                        <h2 class="heading"><?php echo get_sub_field('heading'); ?></h2>
                        <p class="sub-para">
                            <?php echo get_sub_field('text'); ?>
                        </p>
                        <a class="book-btn"><?php echo get_sub_field('button'); ?></a>
                    </div>
                </div>
            </div>

            <div class="container p-rel" style="z-index: 2">
                <div class="row p-0">
                    <div class="col-12 p-0">
                        <div class="arrws-slick">
                            <a class="prev-ar"></a>
                            <a class="next-ar"></a>
                        </div>
                        <div class="what-we-do-carousel">
                            <?php if (have_rows('services_slider')) : ?>
                                <?php while (have_rows('services_slider')) : the_row(); ?>
                                    <div class="top-icon-box2">
                                        <img loading="lazy" src="<?php echo get_sub_field('image'); ?>" alt="Copywriting" title="Copywriting" />
                                        <h3 class="carousel-heading"><?php echo get_sub_field('heading'); ?></h3>
                                    </div>
                                <?php endwhile; ?>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-fluid p-rel" style="z-index: 1">
                <div class="circle4"></div>
                <img loading="lazy" src="<?php echo get_sub_field('arrow_image'); ?>" class="arwOnCircle" alt="Arrow" title="Arrow" />
            </div>
            <div class="container">
                <?php if (have_rows('platforms')) : ?>
                    <?php while (have_rows('platforms')) : the_row(); ?>
                        <div class="row justify-content-center">
                            <div class="col-6 text-center">
                                <div class="img-N img-N-platform">
                                    <img loading="lazy" src="<?php echo get_template_directory_uri(); ?>/assets/img/blackN.png" alt="Platforms we use" title="Platforms we use" />
                                </div>

                                <h2 class="heading"><?php echo get_sub_field('heading'); ?></h2>
                            </div>
                        </div>
                        <div class="row justify-content-around">
                            <div class="col-md-10 col-12">
                                <div class="row">
                                    <?php if (have_rows('platform_row_1')) : ?>
                                        <?php while (have_rows('platform_row_1')) : the_row(); ?>
                                            <div class="col-md-3 col-6">
                                                <img loading="lazy" src="<?php echo get_sub_field('image'); ?>" class="img-fluid" alt="amazon Ads" title="Amazon Ads" />
                                            </div>
                                        <?php endwhile; ?>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                        <div class="row justify-content-center">
                            <?php if (have_rows('platform_row_2')) : ?>
                                <?php while (have_rows('platform_row_2')) : the_row(); ?>
                                    <div class="col colA d-flex align-items-center">
                                        <img loading="lazy" src="<?php echo get_sub_field('image'); ?>" class="img-fluid" alt="google Ads" title="Google Ads" />
                                    </div>
                                <?php endwhile; ?>
                            <?php endif; ?>
                        </div>
                    <?php endwhile; ?>
                <?php endif; ?>
            </div>
        </section>
    <?php endwhile; ?>
<?php endif; ?>

<!-- Our Work -->

<?php if (have_rows('our_work')) : ?>
    <?php while (have_rows('our_work')) : the_row(); ?>
        <section class="our-work">
            <div class="container">
                <div class="row">
                    <div class="col-12 text-center">
                        <div class="img-N2">
                            <img loading="lazy" src="<?php echo get_sub_field('image'); ?>" alt="" />
                        </div>
                        <p class="subheading"><?php echo get_sub_field('sub_heading'); ?></p>
                        <h2 class="heading"><?php echo get_sub_field('heading'); ?></h2>
                    </div>
                </div>

                <?php if (have_rows('tabs')) : ?>

                    <div class="row justify-content-center">
                        <div class="col-lg-10 col-12 work-slider-row">
                            <div class="work-slider row justify-content-around">
                                <?php $i = 1; ?>
                                <?php while (have_rows('tabs')) : the_row(); ?>
                                    <div class="work-company col-2">
                                        <a class="acc <?php if ($i == 3) : ?> active <?php endif; ?>" onclick="playVideo('vid1')" data-val="<?php echo str_replace(" ", "-", strtolower(get_sub_field('tab_heading'))); ?>"><?php echo get_sub_field('tab_heading'); ?></a>
                                    </div>
                                    <?php $i++; ?>
                                <?php endwhile; ?>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-1 text-center my-auto p-rel">
                            <?php //while (have_rows('tabs')) : the_row(); 
                            ?>

                            <!-- <div class="arr-prev eatable-category3 active"></div>

                            <div class="arr-prev edtech-category3"></div>

                            <div class="arr-prev healthcare-category3"></div>

                            <div class="arr-prev fashion-category3"></div>

                            <div class="arr-prev education-category3"></div>

                            <div class="arr-prev b2b-category3"></div> -->
                            <?php //endwhile; 
                            ?>
                        </div>

                        <div class="col-md-10">
                            <div class="tab-panel">
                                <?php $i = 1; ?>
                                <?php while (have_rows('tabs')) : the_row(); ?>
                                    <div class="tabs-outer-div <?php if ($i == 3) : ?> active <?php endif; ?>" id="<?php echo str_replace(" ", "-", strtolower(get_sub_field('tab_heading'))); ?>">
                                        <div class="tabs tabing-slick">
                                            <?php while (have_rows('tab_content')) : the_row(); ?>
                                                <div class="tab-list">
                                                    <img loading="lazy" src="<?php echo get_sub_field('image'); ?>" draggable="false" class="img-fluid" alt="" />
                                                </div>
                                            <?php endwhile; ?>
                                        </div>
                                    </div>
                                    <?php $i++; ?>
                                <?php endwhile; ?>

                                <!-- the opacity section -->

                                <div class="opacityDiv" draggable="false">
                                    <div class="opac-div1 d-none d-md-block">
                                        <img loading="lazy" src="<?php echo get_template_directory_uri(); ?>/assets/img/work/eatable2.jpg" draggable="false" class="img-fluid visible-0" alt="" />
                                    </div>

                                    <div class="opac-div1">
                                        <img loading="lazy" src="<?php echo get_template_directory_uri(); ?>/assets/img/work/eatable2.jpg" draggable="false" class="img-fluid visible-0" alt="" />
                                    </div>

                                    <div class="opac-div2">
                                        <img loading="lazy" src="<?php echo get_template_directory_uri(); ?>/assets/img/iPhone-X.png" class="img-fluid" alt="" />
                                    </div>

                                    <div class="opac-div1">
                                        <img loading="lazy" src="<?php echo get_template_directory_uri(); ?>/assets/img/work/eatable2.jpg" draggable="false" class="img-fluid visible-0" alt="" />
                                    </div>

                                    <div class="opac-div1 d-none d-md-block">
                                        <img loading="lazy" src="<?php echo get_template_directory_uri(); ?>/assets/img/work/eatable2.jpg" draggable="false" class="img-fluid visible-0" alt="" />
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-1 text-center my-auto p-rel">
                            <!-- <div class="arr-next eatable-category2 active"></div>

                            <div class="arr-next edtech-category2"></div>

                            <div class="arr-next healthcare-category2"></div>

                            <div class="arr-next fashion-category2"></div>

                            <div class="arr-next education-category2"></div>

                            <div class="arr-next b2b-category2"></div> -->
                        </div>
                    </div>
                <?php endif; ?>
            </div>
        </section>
    <?php endwhile; ?>
<?php endif; ?>

<!-- WHen To hire Us -->

<?php if (have_rows('hire_section')) : ?>
    <?php while (have_rows('hire_section')) : the_row(); ?>
        <section class="when-to-hire-us">
            <div class="container">
                <div class="row">
                    <div class="col-12 text-center">
                        <div class="img-N2">
                            <img loading="lazy" src="<?php echo get_sub_field('image'); ?>" alt="Hire Performance Marketing Agency in India" title="Hire Performance Marketing Agency in India" />
                        </div>
                        <p class="subheading"><?php echo get_sub_field('sub_heading'); ?></p>
                        <h2 class="heading"><?php echo get_sub_field('heading'); ?></h2>
                    </div>
                </div>
                <div class="row"><?php if (have_rows('hire')) : ?>
                        <?php while (have_rows('hire')) : the_row(); ?>
                            <div class="col-lg-4">
                                <span class="arrow-span">
                                    <i class="fas fa-arrow-right"></i>
                                </span>
                                <div class="when-hire-us">
                                    <h5><?php echo get_sub_field('heading'); ?></h5>
                                    <p>
                                        <?php echo get_sub_field('text'); ?>
                                    </p>
                                </div>
                            </div>
                        <?php endwhile; ?>
                    <?php endif; ?>
                </div>

                <!-- <div class="row">
                    <div class="col-lg-4">
                        <span class="arrow-span">
                            <i class="fas fa-arrow-right"></i>
                        </span>

                        <div class="when-hire-us">
                            <h5>I am not doing Email Marketing for my business</h5>

                            <p>
                                That’s literally putting 30% of Your Total Revenue Potential on
                                the table and walking away. Yes, Klaviyo released the Stats that
                                the Benchmark Ecom Store makes >30% of their Total Revenue only
                                from Email Marketing Flows and Campaigns.
                            </p>
                        </div>
                    </div>

                    <div class="col-lg-4">
                        <span class="arrow-span">
                            <i class="fas fa-arrow-right"></i>
                        </span>

                        <div class="when-hire-us">
                            <h5>I have a Good Team, they need Coaching.</h5>

                            <p>
                                I believe you and your team. Our Skills are 100% teachable to a
                                Good Team. We have a Perfect Plan & Structure for you to gain
                                the same results with your own team. Let’s get on a call to take
                                this forward.
                            </p>
                        </div>
                    </div>

                    <div class="col-lg-4">
                        <span class="arrow-span">
                            <i class="fas fa-arrow-right"></i>
                        </span>

                        <div class="when-hire-us">
                            <h5>I am Fascinated and want to Give you a Shot, anyways.</h5>

                            <p>
                                Thank You!! We are worth every Penny you Pay Us. Let’s get on a
                                call and see how we can kickstart your Digital Growth.
                            </p>
                        </div>
                    </div>
                </div> -->

                <div class="row justify-content-center">
                    <div class="col-lg-4 text-center">
                        <a class="book-btn"><?php echo get_sub_field('button'); ?></a>
                    </div>
                </div>
            </div>
        </section>
    <?php endwhile; ?>
<?php endif; ?>

<?php if (have_rows('pdf_section')) : ?>
    <?php while (have_rows('pdf_section')) : the_row(); ?>
        <section class="when-to-hire-us" style="background: #181817; padding: 50px 0 50px 0">
            <div class="container">
                <div class="row">
                    <div class="col-12 text-center">
                        <h2 class="heading" style="color: white">
                            <?php echo get_sub_field('heading'); ?>
                        </h2>
                    </div>
                </div>

                <div class="row justify-content-center" style="margin: 0px !important">
                    <div class="col-lg-3 text-center" style="margin-top: 20px">
                        <?php
                        $link = get_sub_field('download');
                        if ($link) :
                            $link_url = $link['url'];
                            $link_title = $link['title'];
                            $link_target = $link['target'] ? $link['target'] : '_self';
                        ?>
                            <a class="pdf-btn" href=" <?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>
                        <?php endif; ?>

                        <!-- <a class="pdf-btn" href="https://neondigital.media/Amazon_Exports_Digest_2020.pdf" download>Download PDF</a> -->
                    </div>

                    <div class="col-lg-3 text-center" style="margin-top: 20px">
                        <?php
                        $link = get_sub_field('share');
                        if ($link) :
                            $link_url = $link['url'];
                            $link_title = $link['title'];
                            $link_target = $link['target'] ? $link['target'] : '_self';
                        ?>
                            <a class="pdf-btn" href=" <?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>
                        <?php endif; ?>

                        <!-- <a class="pdf-btn" href="https://api.whatsapp.com/send?text=https://drive.google.com/file/d/1R7bnzS144l8l4a_dhnZwBYIoCbgUoxPW/view?usp=sharing">Share The PDF</a> -->
                    </div>
                </div>
            </div>
        </section>
    <?php endwhile; ?>
<?php endif; ?>

<!-- Frequently Asked Questions -->

<?php if (have_rows('faq_section')) : ?>
    <?php while (have_rows('faq_section')) : the_row(); ?>
        <section class="faq" style="padding-top: 100px">
            <div class="container">
                <div class="row">
                    <div class="col-12 text-center">
                        <div class="img-N2">
                            <img loading="lazy" src="<?php echo get_sub_field('image'); ?>" alt="Performance Marketing Frequently Asked Questions" title="Performance Marketing FAQ's" />
                        </div>
                        <p class="subheading"><?php echo get_sub_field('sub_heading'); ?></p>
                        <h2 class="heading"><?php echo get_sub_field('heading'); ?></h2>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6">
                        <div class="accordion">
                            <?php if (have_rows('faq_one')) : ?>
                                <?php while (have_rows('faq_one')) : the_row(); ?>
                                    <h6 class="accordion-toggle">
                                        <?php echo get_sub_field('question'); ?><i class="fa fa-angle-right accordion-angle"></i>
                                    </h6>
                                    <div class="accordion-content">
                                        <p>
                                            <?php echo get_sub_field('answer'); ?>
                                        </p>
                                    </div>
                                <?php endwhile; ?>
                            <?php endif; ?>
                        </div>
                    </div>

                    <div class="col-lg-6">
                        <div class="accordion">
                            <?php if (have_rows('faq_two')) : ?>
                                <?php while (have_rows('faq_two')) : the_row(); ?>
                                    <h6 class="accordion-toggle">
                                        <?php echo get_sub_field('question'); ?>
                                        <i class="fa fa-angle-right accordion-angle"></i>
                                    </h6>
                                    <div class="accordion-content">
                                        <p>
                                            <?php echo get_sub_field('answer'); ?>
                                        </p>
                                    </div>
                                <?php endwhile; ?>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    <?php endwhile; ?>
<?php endif; ?>

<?php
get_footer();
?>