var bookbtn = document.querySelectorAll(".book-btn");

for (j = 0; j < bookbtn.length; j++) {
  bookbtn[j].addEventListener("click", () => {
    document.getElementById("viewPoint").scrollIntoView({ behavior: "smooth" });
  });
}

(function ($) {
  $(document).ready(function () {
    console.log("hello");

    $(".about-carousel").slick({
      slidesToShow: 3,

      slidesToScroll: 1,

      infinite: false,

      loop: true,

      autoplaySpeed: 2000,

      responsive: [
        {
          breakpoint: 480,

          settings: {
            slidesToShow: 1,

            slidesToScroll: 1,
          },
        },
      ],
    });

    $(".what-we-do-carousel").slick({
      variableWidth: true,

      slidesToScroll: 1,

      infinite: true,

      loop: true,

      autoplaySpeed: 2000,

      prevArrow: $(".prev-ar"),

      nextArrow: $(".next-ar"),

      responsive: [
        {
          breakpoint: 480,

          settings: {
            slidesToShow: 1,

            slidesToScroll: 1,
          },
        },
      ],
    });

    $(".tabs1").slick({
      slidesToShow: 5,

      slidesToScroll: 1,

      infinite: true,

      loop: true,

      autoplaySpeed: 2000,

      prevArrow: $(".edtech-category3"),

      nextArrow: $(".edtech-category2"),

      responsive: [
        {
          breakpoint: 480,

          settings: {
            variableWidth: true,

            centerMode: true,
          },
        },
      ],
    });

    $(".tabs2").slick({
      slidesToShow: 5,

      slidesToScroll: 1,

      infinite: true,

      loop: true,

      autoplaySpeed: 2000,

      prevArrow: $(".healthcare-category3"),

      nextArrow: $(".healthcare-category2"),

      responsive: [
        {
          breakpoint: 480,

          settings: {
            variableWidth: true,

            centerMode: true,
          },
        },
      ],
    });

    $(".tabs3").slick({
      slidesToShow: 5,

      slidesToScroll: 1,

      infinite: true,

      loop: true,

      autoplaySpeed: 2000,

      prevArrow: $(".eatable-category3"),

      nextArrow: $(".eatable-category2"),

      responsive: [
        {
          breakpoint: 480,

          settings: {
            variableWidth: true,

            centerMode: true,
          },
        },
      ],
    });

    $(".tabs4").slick({
      slidesToShow: 5,

      slidesToScroll: 1,

      infinite: true,

      loop: true,

      autoplaySpeed: 2000,

      prevArrow: $(".fashion-category3"),

      nextArrow: $(".fashion-category2"),

      responsive: [
        {
          breakpoint: 480,

          settings: {
            variableWidth: true,

            centerMode: true,
          },
        },
      ],
    });

    $(".tabs5").slick({
      slidesToShow: 5,

      slidesToScroll: 1,

      infinite: true,

      loop: true,

      autoplaySpeed: 2000,

      prevArrow: $(".b2b-category3"),

      nextArrow: $(".b2b-category2"),

      responsive: [
        {
          breakpoint: 480,

          settings: {
            variableWidth: true,

            centerMode: true,
          },
        },
      ],
    });

    $(".tabs6").slick({
      slidesToShow: 5,

      slidesToScroll: 1,

      infinite: true,

      loop: true,

      autoplaySpeed: 2000,

      prevArrow: $(".education-category3"),

      nextArrow: $(".education-category2"),

      responsive: [
        {
          breakpoint: 480,

          settings: {
            variableWidth: true,

            centerMode: true,
          },
        },
      ],
    });

    /* new */

    $(".tabing-slick").slick({
      slidesToShow: 5,

      slidesToScroll: 1,

      infinite: true,

      loop: true,

      autoplaySpeed: 2000,

      // prevArrow: $(".eatable-category3"),

      // nextArrow: $(".eatable-category2"),

      responsive: [
        {
          breakpoint: 480,

          settings: {
            variableWidth: true,

            centerMode: true,
          },
        },
      ],
    });
  });

  $(function () {
    $(".col-md-3").slice(0, 8).show();

    $("body").on("click touchstart", ".load-more", function (e) {
      e.preventDefault();

      $(".col-md-3:hidden").slice(0, 4).slideDown();

      if ($(".col-md-3:hidden").length == 0) {
        $(".load-more").css("visibility", "hidden");
      }
    });
  });

  $(".accordion")
    .find(".accordion-toggle")
    .click(function () {
      $(this).next().slideToggle("600");

      $(this).siblings(".accordion-content").not($(this).next()).slideUp("600");
    });

  $(".accordion-toggle").on("click", function () {
    $(this).toggleClass("active").siblings().removeClass("active");
  });

  $(".work-company > a").click(function () {
    $(".work-company > a").removeClass("active");

    $(this).addClass("active");

    $(".tab-panel .tabs-outer-div").removeClass("active");

    var hh = $(this).attr("data-val");

    var jj = "#" + hh;

    // var jj2 = hh + "2";

    // var jj3 = hh + "3";

    $(jj).addClass("active");

    console.log();

    // $(".arr-next").removeClass("active");

    // $("." + jj2).addClass("active");

    // $(".arr-prev").removeClass("active");

    // $("." + jj3).addClass("active");
  });
})(jQuery);

function playVideo() {
  $(document).ready(function () {
    $("video").pause();
  });

  for (var i = 0; i < arguments.length; i++) {
    var vid = document.getElementById(arguments[i]);

    vid.play();
  }
}
