<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>
    <meta <?php bloginfo('charset'); ?>>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/assets/img/Favicon.jpg" type="image/png">

    <!-- you html website meta tags-->

    <!-- <meta http-equiv="content-type" content="text/html;charset=UTF-8" />

    <meta charset="utf-8" />

    <meta http-equiv="content-language" content="en" />

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate, max-age=0" />

    <meta http-equiv="Pragma" content="no-cache" />

    <meta http-equiv="Expires" content="0" />

    <meta name="theme-color" content="#fff" />

    <title>
        Neon Digital Media- A Full Funnel Performance Marketing Agency India
    </title>

    <meta name="description" content="An Army of Data Driven Marketers for Businesses looking to Scale Online, Meher Patel Founder Strategist at Neondigitalmedia" />

    <link rel="canonical" href="https://www.neondigital.media/" />


    <link rel="icon" href="img/Favicon.jpg" />

    

    <meta property="og:title" content="Neon Digital Media- A Full Funnel Performance Marketing Agency India" />

    <meta property="og:description" content="An Army of Data Driven Marketers for Businesses looking to Scale Online." />

    <meta property="og:url" content="https://www.neondigital.media/" />

    <meta property="og:image" content="../neondigital.media/images/neongreen.png" />

    <meta property="og:type" content="website" />

    <meta property="og:locale" content="en_US" />

    


    <meta name="twitter:card" content="summary_large_image" />

    <meta property="twitter:domain" content="neondigital.media" />

    <meta property="twitter:url" content="https://www.neondigital.media/" />

    <meta name="twitter:title" content="Neon Digital Media- A Full Funnel Performance Marketing Agency India" />

    <meta name="twitter:description" content="An Army of Data Driven Marketers for Businesses looking to Scale Online." />

    <meta name="twitter:image" content="../neondigital.media/images/front-image.html" />

    

    <meta name="dc.title" content="Neon Digital Media- A Full Funnel Performance Marketing Agency India" />

    <meta name="dc.description" content="An Army of Data Driven Marketers for Businesses looking to Scale Online." />

    <meta name="dc.subject" content="An Army of Data Driven Marketers for Businesses looking to Scale Online." />

    <meta name="dc.source" content="neondigital.media" />

    <meta content="global" name="distribution" />

    <meta content="Yes" name="allow-search" />

    <meta name="coverage" content="Worldwide" />

    <meta name="classification" content="Content" />

    <meta name="rating" content="general" />

    <meta name="revisit-after" content="1 days" />

    <meta name="robots" content="noodp" />

    <meta name="distribution" content="global" />

    <meta name="document-rating" content="Safe for Kids" />

    <meta name="document-type" content="Public" />

    <meta name="expires" content="never" />

    <meta name="HandheldFriendly" content="True" />

    <meta name="target" content="all" />

    <meta name="YahooSeeker" content="Index,Follow" />

    <meta name="generator" content="neondigital.media" />

    <meta name="abstract" content="NEON Digital - Neon Digital Media- A Full Funnel Performance Marketing Agency India" />

    <meta name="author" content="NEON Digital - Neon Digital Media- A Full Funnel Performance Marketing Agency India" />

    <meta name="copyright" content="© 2021 NEON Digital - Neon Digital Media- A Full Funnel Performance Marketing Agency India" />

    <meta name="subject" content="NEON Digital - Neon Digital Media- A Full Funnel Performance Marketing Agency India" />

    <meta name="address" content="Wework, Vi-John Tower, 393, Phase III, Gurugram, Haryana 122016" />

    <meta name="country" content="India" />

    <meta name="State" content="Haryana" />

    <meta name="city" content="Gurgaon" />

    <meta name="geo.placename" content="India" />

    <meta name="geo.position" content="28.5058953,77.0890259,15z" />

    <meta name="geo.region" content="IN" />

    <meta name="ICBM" content="28.5058953,77.0890259,15z" /> -->


    <title>Neon || <?php echo get_the_title(); ?></title>
    <?php wp_head(); ?>

    <!-- Google Tag Manager -->

    <script>
        (function(w, d, s, l, i) {
            w[l] = w[l] || [];
            w[l].push({
                "gtm.start": new Date().getTime(),
                event: "gtm.js",
            });
            var f = d.getElementsByTagName(s)[0],
                j = d.createElement(s),
                dl = l != "dataLayer" ? "&l=" + l : "";
            j.async = true;
            j.src = "../www.googletagmanager.com/gtm5445.html?id=" + i + dl;
            f.parentNode.insertBefore(j, f);
        })(window, document, "script", "dataLayer", "GTM-N3BCQQS");
    </script>

    <!-- End Google Tag Manager -->
</head>

<body <?php body_class(); ?>>
    <!-- Google Tag Manager (noscript) -->

    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-N3BCQQS" height="0" width="0" style="display: none; visibility: hidden"></iframe></noscript>

    <!-- End Google Tag Manager (noscript) -->
    <header>
        <div class="container z-in2 p-rel">
            <div class="row">
                <div class="col-lg-6 col-5 my-auto">
                    <div class="logo">
                        <img loading="lazy" src="<?php echo get_field('logo', 'option'); ?>" alt="Neon Digital Media" title="Neon Digital Media" />
                    </div>
                </div>
                <div class="col-lg-6 col-7 text-right">
                    <a class="book-btn"><?php echo get_field('Button', 'option'); ?></a>
                </div>
            </div>
        </div>
    </header>