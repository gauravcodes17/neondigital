<?php

get_header();
?>

<!---------- Blog Detailed Content Start ---------->

<div class="blog-detailed">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-12">
                <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                        <?php wpb_set_post_views(get_the_ID()); ?>
                        <div class="blog-detailing">
                            <h1><?php echo get_the_title(); ?></h1>
                            <span><img src="<?php echo get_template_directory_uri(); ?>/assets/img/calender.png" class="img-fluid" alt="date"><?php echo get_the_date('F d, Y'); ?></span>
                            <div class="blog-detail-content">
                                <?php echo get_the_content(); ?>
                            </div>
                            <!-- <div class="blog-detailed-social-icons">
                                <?php //echo do_shortcode('[Sassy_Social_Share]'); ?>
                            </div> -->
                        </div>
                        <div class="author">
                            <div class="row">
                                <div class="col-md-3 col-12">
                                    <div class="author-l">
                                        <?php if (get_avatar(get_the_author_meta('ID')) !== FALSE) : ?>
                                            <?php $authorName = get_the_author(); ?>
                                            <?php echo get_avatar(get_the_author_meta('ID'), 150, '', $authorName); ?>
                                        <?php endif; ?>
                                    </div>
                                </div>
                                <div class="col-md-9 col-12">
                                    <div class="author-content">
                                        <h6>About the Author</h6>
                                        <p><?php echo get_the_author_meta('description'); ?></p>
                                        <?php //if (do_shortcode('[author-social-links]') !== "") : ?>
                                            <!-- <div>
                                                <span>Follow Writer:</span>
                                                <span>
                                                    <?php //echo do_shortcode('[author-social-links]'); ?>
                                                </span>
                                            </div> -->
                                        <?php //endif; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endwhile; ?>
                <?php endif; ?>
            </div>
            <div class="col-md-4 col-12">
                <div class="blog-right-section">
                    <div class="categories">
                        <?php dynamic_sidebar('blog-categories-sidebar'); ?>
                    </div>
                    <div class="latest-posts">
                        <?php dynamic_sidebar('recent-post-sidebar'); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!---------- Blog Detailed Content End ---------->

<?php
get_footer();
?>