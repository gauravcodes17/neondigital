<?php

/***
 * Template Name: Blog Page Template
 */
get_header();
?>

<div class="blog-banner" style="background-image: url(<?php echo get_field("blog_background_image") ?>);">
    <div class="bb-overlay"></div>
    <!-- <img src="<?php //echo get_template_directory_uri(); ?>/assets/img/blog-banner2.jpg" class="img-fluid w-100" alt=""> -->
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h4>Blogs</h4>
            </div>
        </div>
    </div>
</div>

<div class="blogs-section">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-12">
                <div class="main-blog-wrapper">
                    <h1><?php echo get_field('main_heading'); ?></h1>
                    <?php
                    $paged = get_query_var('paged') ? get_query_var('paged') : 1;
                    $args = array(
                        'paged' => $paged,
                        'post_type'   => 'post',
                        'posts_per_page' => 2,
                        'post_status' => 'publish',
                        'order'    => 'DESC',
                        'orderby'    => 'ID',
                    );
                    $query = new WP_Query($args);

                    if ($query->have_posts()) :
                    ?>
                        <div class="row">
                            <?php while ($query->have_posts()) : $query->the_post(); ?>
                                <?php $cats = get_the_category(get_the_ID()); ?>
                                <div class="col-lg-6 col-12">
                                    <div class="blog-wrap">
                                        <a href="<?php echo get_the_permalink(); ?>">
                                            <img src="<?php echo get_the_post_thumbnail_url(); ?>" class="img-fluid main-blog-image w-100" alt="<?php echo get_the_title(); ?>">
                                        </a>
                                        <div class="blog-content">
                                            <div>
                                                <span><img src="<?php echo get_template_directory_uri(); ?>/assets/img/calender.png" class="img-fluid" alt="date"><?php echo get_the_date('F d, Y'); ?></span>
                                                <!-- <span>2 Min Read</span> -->
                                            </div>
                                            <a href="<?php echo get_the_permalink(); ?>">
                                                <h5><?php echo get_the_title(); ?></h5>
                                            </a>
                                            <p><?php echo get_the_excerpt(); ?></p>
                                        </div>
                                        <div class="category-name"><?php echo $cats[0]->name; ?></div>
                                    </div>
                                </div>
                            <?php endwhile; ?>

                        </div>
                    <?php endif; ?>
                </div>
                <div class="row custom-page-navi">
                    <div class="col-md-12">
                        <div class="prev-next">
                            <?php wp_pagenavi(array('query' => $query)); ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-12">
                <div class="blog-right-section">
                    <div class="categories">
                        <?php dynamic_sidebar('blog-categories-sidebar'); ?>
                    </div>
                    <div class="latest-posts">
                        <?php dynamic_sidebar('recent-post-sidebar'); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
get_footer();
?>