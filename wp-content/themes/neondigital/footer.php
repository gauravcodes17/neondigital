<footer>
    <div class="circle5"></div>
    <div class="footer-bgText">
        <img loading="lazy" src="<?php echo get_field('footer_background_image', 'option'); ?>" class="<?php echo get_template_directory_uri(); ?>/assets/img-fluid" alt="Neon Digital Media" title="Neon Digital Media" />
    </div>
    <div class="container p-rel z-in2">
        <div class="row">
            <div class="col-lg-12">
                <p class="subheading"><?php echo get_field('sub_heading', 'option'); ?></p>

                <h2 class="heading text-white"><?php echo get_field('heading', 'option'); ?></h2>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-lg-5 wp-form">
                <?php //echo get_field('footer_form_code', 'option'); 
                ?>
                <div id="viewPoint">
                    <?php echo do_shortcode('[contact-form-7 id="900f49f" title="Contact Form Footer"]');
                    ?>
                </div>
                <!-- <form class=" footform" jsCall="footform" id="viewPoint">
                    <input type="text" class="form-control" name="name" placeholder="Your Name" />

                    <input type="email" name="email" class="form-control" id="email" placeholder="Email Address" />

                    <input type="button" class="form-submit footer-submit-btn" value="Book a free consultation" />

                    <p class="error_message text-danger text-center mt-2"></p>

                    <p class="success_message text-success text-center mt-2"></p>
                    </form> -->
            </div>
        </div>

        <style>
            .locall a {
                color: #eee;
            }

            .localsep {
                color: #7ec453;
            }
        </style>
        <div class="row">
            <div class="col-lg-12 locall">
                <p class="text-center">
                    <small><?php
                            $link = get_field('link_1', 'option');
                            if ($link) :
                                $link_url = $link['url'];
                                $link_title = $link['title'];
                                $link_target = $link['target'] ? $link['target'] : '_self';
                            ?>
                            <a href=" <?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>
                        <?php endif; ?>
                        <span class="localsep">|</span>
                        <?php
                        $link = get_field('link_2', 'option');
                        if ($link) :
                            $link_url = $link['url'];
                            $link_title = $link['title'];
                            $link_target = $link['target'] ? $link['target'] : '_self';
                        ?>
                            <a href=" <?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>
                        <?php endif; ?>
                        <span class="localsep">|</span>
                        <?php
                        $link = get_field('link_3', 'option');
                        if ($link) :
                            $link_url = $link['url'];
                            $link_title = $link['title'];
                            $link_target = $link['target'] ? $link['target'] : '_self';
                        ?>
                            <a href=" <?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>
                        <?php endif; ?>
                        <span class="localsep">|</span>
                        <?php
                        $link = get_field('link_4', 'option');
                        if ($link) :
                            $link_url = $link['url'];
                            $link_title = $link['title'];
                            $link_target = $link['target'] ? $link['target'] : '_self';
                        ?>
                            <a href=" <?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>
                        <?php endif; ?>
                    </small>
                </p>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 locall">
                <p class="text-center">
                    <small>
                        <?php
                        $link = get_field('link_5', 'option');
                        if ($link) :
                            $link_url = $link['url'];
                            $link_title = $link['title'];
                            $link_target = $link['target'] ? $link['target'] : '_self';
                        ?>
                            <a href=" <?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>
                        <?php endif; ?>
                        <span class="localsep">|</span>
                        <?php
                        $link = get_field('link_6', 'option');
                        if ($link) :
                            $link_url = $link['url'];
                            $link_title = $link['title'];
                            $link_target = $link['target'] ? $link['target'] : '_self';
                        ?>
                            <a href=" <?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>
                        <?php endif; ?>
                    </small>
                </p>
            </div>
        </div>

        <!-- <div class="row">
            <div class="col-lg-12 locall">
                <p class="text-center">
                    <small><a href="gurgaon.php">Gurgaon</a>
                        <span class="localsep">|</span> <a href="delhi.php">Delhi</a>
                        <span class="localsep">|</span> <a href="mumbai.php">Mumbai</a>
                        <span class="localsep">|</span> <a href="pune.php">Pune</a>
                    </small>
                </p>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 locall">
                <p class="text-center">
                    <small><a href="/news-and-media">NEWS & Media</a>
                        <span class="localsep">|</span> <a href="/blogs/">Blogs</a>
                    </small>
                </p>
            </div>
        </div> -->

    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-4 my-auto">
                            <p class="text-white my-auto" style="font-size: 12px">
                                <?php echo get_field('copyright', 'option'); ?>
                            </p>
                        </div>

                        <div class="col-lg-4 text-center">
                            <img loading="lazy" src="<?php echo get_field('footer_logo', 'option'); ?>" alt="Neon Digital Media" title="Neon Digital Media" />
                        </div>

                        <div class="col-lg-2 my-auto text-center">
                            <?php
                            $link = get_field('facebook_link', 'option');
                            if ($link) :
                                $link_url = $link['url'];
                                $link_title = $link['title'];
                                $link_target = $link['target'] ? $link['target'] : '_self';
                            ?>
                                <a href=" <?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>"><i class="fab fa-facebook footer-social"></i></a>
                            <?php endif; ?>

                            <!-- <a href="https://www.facebook.com/NeonDigitalMedia" target="_blank"><i class="fab fa-facebook footer-social"></i></a> -->
                        </div>

                        <div class="col-lg-2 my-auto text-center">
                            <?php
                            $link = get_field('privacy_policy', 'option');
                            if ($link) :
                                $link_url = $link['url'];
                                $link_title = $link['title'];
                                $link_target = $link['target'] ? $link['target'] : '_self';
                            ?>
                                <a href=" <?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>
                            <?php endif; ?>

                            <!-- <a href="privacypolicy.html" target="_blank">Privacy Policy</a> -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>

<?php
wp_footer();
?>