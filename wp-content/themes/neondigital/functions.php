<?php

/**
 * Functions and definitions
 */



// Exit-if accessed directly
if (!defined('ABSPATH'))
    exit;

/* THEME OPTIONS PAGE - HEADER, FOOTER, FAQS, CONSULTATION */
if (function_exists('acf_add_options_page')) {

    acf_add_options_page(
        array(
            'page_title' => 'Theme Options',
            'menu_title' => 'Theme Options',
            'menu_slug' => 'theme-options',
            'capability' => 'edit_posts',
            'parent_slug' => '',
            'position' => false,
            'icon_url' => false
        )
    );

    acf_add_options_sub_page(
        array(
            'page_title' => 'Header',
            'menu_title' => 'Header',
            'menu_slug' => 'theme-options-header',
            'capability' => 'edit_posts',
            'parent_slug' => 'theme-options',
            'position' => false,
            'icon_url' => false
        )
    );

    acf_add_options_sub_page(
        array(
            'page_title' => 'Footer',
            'menu_title' => 'Footer',
            'menu_slug' => 'theme-options-footer',
            'capability' => 'edit_posts',
            'parent_slug' => 'theme-options',
            'position' => false,
            'icon_url' => false
        )
    );

    // acf_add_options_sub_page(
    //     array(
    //         'page_title' => 'Common Sections',
    //         'menu_title' => 'Common Sections',
    //         'menu_slug' => 'theme-options-common',
    //         'capability' => 'edit_posts',
    //         'parent_slug' => 'theme-options',
    //         'position' => false,
    //         'icon_url' => false
    //     )
    // );
}

function add_theme_scripts()
{

    /* Styles */
    wp_enqueue_style('bootstrap', get_template_directory_uri() . '/assets/css/bootstrap.min.css', false, '1.0', 'all');
    wp_enqueue_style('fontawesome', get_template_directory_uri() . '/assets/css/fontawesome.min.css', false, '1.0', 'all');
    wp_enqueue_style('slick', get_template_directory_uri() . '/assets/css/slick.css', false, '1.0', 'all');
    wp_enqueue_style('slick-theme', get_template_directory_uri() . '/assets/css/slick-theme.css', false, '1.0', 'all');
    wp_enqueue_style('main', get_template_directory_uri() . '/assets/css/main.css', false, '1.0', 'all');


    

    /* Scripts */

    if (!is_admin()) {
        //Call JQuery
        //wp_deregister_script('jquery');
    }
    wp_enqueue_script('jquery', get_template_directory_uri() . '/assets/js/jquery-3.6.0.min.js', array(), null, true);
    wp_enqueue_script('bootstrap', get_template_directory_uri() . '/assets/js/bootstrap-bundle.min.js', array(), null, true);
    wp_enqueue_script('fontawesome', get_template_directory_uri() . '/assets/js/fontawesome.min.js', array(), null, true);
    wp_enqueue_script('slick-theme', get_template_directory_uri() . '/assets/js/slick.min.js', array(), null, true);
    wp_enqueue_script('myjs', get_template_directory_uri() . '/assets/js/my2.js', array(), null, true);
}
add_action('wp_enqueue_scripts', 'add_theme_scripts');

// Add Theme Support
add_theme_support('post-thumbnails');

function neon_init()
{
    register_sidebar(
        array(
            'name' => esc_html__('Blog Categories Sidebar', 'neon'),
            'id' => 'blog-categories-sidebar',
            'description' => esc_html__('Add widgets here to appear in details blog page.', 'neon'),
            'before_widget' => '',
            'after_widget' => '',
            'before_title' => '',
            'after_title' => '',
        )
    );

    register_sidebar(
        array(
            'name' => esc_html__('Recent Post Sidebar', 'neon'),
            'id' => 'recent-post-sidebar',
            'description' => esc_html__('Add widgets here to appear in details blog page.', 'neon'),
            'before_widget' => '',
            'after_widget' => '',
            'before_title' => '',
            'after_title' => '',
        )
    );
}
add_action('widgets_init', 'neon_init');

function wpb_set_post_views($postID)
{
    $count_key = 'wpb_post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if ($count == '') {
        $count = 0;
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
    } else {
        $count++;
        update_post_meta($postID, $count_key, $count);
    }
}
//To keep the count accurate, lets get rid of prefetching
remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);

function wpb_track_post_views($post_id)
{
    if (!is_single()) return;
    if (empty($post_id)) {
        global $post;
        $post_id = $post->ID;
    }
    wpb_set_post_views($post_id);
}
add_action('wp_head', 'wpb_track_post_views');
